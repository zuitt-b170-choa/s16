/*
	Repetition Control Structures
		Loops - executes codes repeatedly in a pre-set number of times or forever.

*/

// While Loop -  take an expression/condition before proceeding in the evaluation of the codes.
	/*
		SYNTAX:
			while(condition){
				statement/s
			}

	*/

let count = 5;
while(count!==0){
	console.log("While-Loop: " + count);

	count --;
};

let countTwo = 1;
while(countTwo < 11){
	console.log(countTwo);
	countTwo++;
}

//Do-While Loop - at least 1 code block to be executed before proceeding to the condition.

	/*
		SYNTAX:
		do{
			statement/s
		}while(condition)
	*/	 

let countThree = 5;
do{
	console.log("Do-While Loop: "+ countThree);
	countThree --;
}while(countThree > 0);

let countFour = 1;
do{
	console.log(countFour);
	countFour++;
}while(countFour<11);

// For Loop -  more flexible looping
	/*
		SYNTAX
		for(initialization; condition; final expression){
			statement/s
		}

		Process:
			1. Initialization - recognizing the variable created/
			2. Condition - reading the condition
			3. Statement/s - executing the commands/statements
			4. Final expression - executing the final expression
	*/

for (let countFive = 5; countFive > 0; countFive --){
	console.log("For Loop: " +countFive);
}

let number = Number(prompt("Give me a number: "));
for(let numCount = 1; numCount <= number; numCount ++){
	console.log("Hello Batch 170!");
}

let myName = "alex";
/*
	.length - representing the number of characters inside the string. the characters that are included in the string (spaces, symbols, punctuations.)
*/
console.log(myName.length);

// variable[x] - accessing the array/string of the variable; the counting the elements starts with 0.
console.log(myName[2]);

for(let x=0; x<myName.length; x++){
	console.log(myName[x]);
}

myName = "Alex";
for(let i=0; i < myName.length; i++){
	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
		){
		console.log(3);
	}else{
		console.log(myName[i]);
	}
}

// Continue and Break Statements

for(let countSix = 0; countSix <=20; countSix++){
	if(countSix % 2 === 0){
		continue; //tells the browser to continue to the next iteration of the loop.
		}
	console.log("Continue and Break: "+ countSix);
	if(countSix>10){
		break; //tells the browser/codes to terminate the loop even if the condition in the loop is met/satisfied.
	}
}

let name = "Alexandro";
// name has 8 indeces/(name[index]) but 9 elements(.length)
/*
	compute for index: length - 1
	compute for length: index +1;
*/

/*
	1. Initializaton - recognizing the variable created
	2. Condition - reading the condition
	3. Statement/s - executing the commands/statements
	4. Final Expression - executing the final Expression
*/

for(let i=0; i<name.length; i++){
	console.log(name[i]);z

	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration.");
		continue;
	}
	if(name[i].toLowerCase() === "d"){
		break;
	}
}