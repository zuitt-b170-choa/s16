

let numProv = parseInt(prompt("Provide a number: "));
console.log(`The number you provided is ${numProv}.`)

for(let x = numProv; x > 0; x--){
	if(x<=50){
		console.log(`The current value is at ${x}. Terminating the loop.`)
		break;
	}
	if(x % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	if(x % 5 === 0){
		console.log(x);
	}
}


let word = "supercalifragilisticexpialidocious";
console.log(word);
let newWord = word.split("");
let final = [];

for(x = 0; x < word.length; x++){
	if( newWord[x] == "a" ||
		newWord[x] == "e" ||
		newWord[x] == "i" ||
		newWord[x] == "o" ||
		newWord[x] == "u"
		){
		continue;
	}
	if(	newWord[x] != "a" ||
		newWord[x] != "e" ||
		newWord[x] != "i" ||
		newWord[x] != "o" ||
		newWord[x] != "u")
	{
		final[x] = newWord[x];
	}
}

console.log(final.join(""));
